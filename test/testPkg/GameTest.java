/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testPkg;

import battleship.Game;

import junit.framework.Assert;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

/**
 *
 * @author James Watson, Nick Watt
 * 
 * The test contained in this class ensure that user input is valid.
 * 
 */
@RunWith(JUnitPlatform.class)
public class GameTest {
    
    @Test
    public void testIsValidGuessOK()
    {
        String input = "A1";
        boolean expResult = true;
        boolean actual = Game.isValidGuess(input);
        
        assertAll("Do many assertions.", () -> {
            assertNotNull(actual);
            assertEquals(expResult, actual);
        });
    }
    
    @Test
    public void testIsValidGuessFail()
    {
        String input = "AA";
        boolean expResult = false;
        boolean actual = Game.isValidGuess(input);
        
        assertAll("Do many assertions.", () -> {
            assertNotNull(actual);
            assertEquals(expResult, actual);
        });
    }
    
    @Test
    public void testIsValidPlaceOK()
    {
        String input = "VA1";
        boolean expResult = true;
        boolean actual = Game.isValidPlace(input);
        
        assertAll("Do many assertions.", () -> {
            assertNotNull(actual);
            assertEquals(expResult, actual);
        });
    }
    
    @Test
    public void testIsValidPlaceFail()
    {
        String input = "AAA";
        boolean expResult = false;
        boolean actual = Game.isValidPlace(input);
        
        assertAll("Do many assertions.", () -> {
            assertNotNull(actual);
            assertEquals(expResult, actual);
        });
    }
    
    @Test
    public void testLetterIndexOK()
    {
        char input = 'a';
        int expResult = 0;
        int actual = Game.letterIndex(input);
        
        assertAll("Do many assertions.", () -> {
            assertNotNull(actual);
            assertEquals(expResult, actual);
        }); 
    }
    
    // using this to see the numbers for various characters
    @Test
    public void testLetterIndexFail()
    {
        char input = '?';
        int expResult = -34;
        int actual = Game.letterIndex(input);
        
        assertAll("Do many assertions.", () -> {
            assertNotNull(actual);
            assertEquals(expResult, actual);
        }); 
    }
    
    @Test
    public void testIndexLetterOK()
    {
        int input = 1;
        String expResult = "B";
        String actual = Game.indexLetter(input);
        
        assertAll("Do many assertions.", () -> {
            assertNotNull(actual);
            assertEquals(expResult, actual);
        }); 
    }
    
    // This test checks if the if statement prevents input higher than Game.len
    @Test
    public void testIndexLetterNull()
    {   
        Assert.assertNull(Game.indexLetter(10));
    }
}
