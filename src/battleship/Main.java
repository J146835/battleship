/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

/**
 *
 * @author James Watson, Nick Watt
 * 
 * Main class sets up the game and processes the turns until the game ends
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // performs basic setup process
        Game.setup();

        // bool for checking the game state
        boolean checkGame = true;
        
        // Turns take place here - Each turn would contain the players actions reflected on the topboard and the CPU's actions reflected on the bottom board
        while(checkGame)
        {   
            Game.separator();
            Game.playerGuess();
            System.out.println(Game.printArray(Game.topBoard));
            Game.opponentGuess();
            System.out.println(Game.printArray(Game.bottomBoard));
            
            // check whether the game is over or not and decides the winner
            if(!Game.checkGame(Game.opponentFleet) || !Game.checkGame(Game.playerFleet))
            {
                checkGame = false;
            }
        }
        
        
        if(!Game.checkGame(Game.opponentFleet) && !Game.checkGame(Game.playerFleet))
        {
            System.out.println("Draw?");
        }
        else if(!Game.checkGame(Game.opponentFleet))
        {
            System.out.println("Congratulations, you have won!");
        }
        else if(!Game.checkGame(Game.playerFleet))
        {
            System.out.println("Sorry, You have lost...");
        }
    } 
}
