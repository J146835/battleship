/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

/**
 *
 * @author James Watson, Nick Watt
 * 
 * Ship class represents the play pieces
 */
public class Ship {
    
    String Name;
    int Hits;
    int Length;
    public Orientation Orient;
    public Owner Own;
    boolean IsAfloat;
    
    // Enum for the defining the ships orientation when placing the ship
    public enum Orientation {
        VERTICAL("Vertical"),
        HORIZONTAL("Horizontal");
        
        private final String label;
        
        Orientation(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }
    
    public enum Owner {
        PLAYER("player"),
        OPPONENT("opponent");
        
        private final String label;
        
        Owner(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }
    
    void setName(String name){
        this.Name = name;
    }
    
    String getName(){
        return Name;
    }
    
    // Checks if the ship is still alive
    public void checkSink()
    {
        IsAfloat = Hits != Length;
        
        if(!IsAfloat)
        {
            if(Own == Owner.OPPONENT)
            {
                System.out.println("You sunk the " + Name + "!");
            }
            else
            {
               System.out.println("Your opponent sunk your " + Name + "!"); 
            }   
        }
    }
    
    // adds a hit to the ship
    public void hit()
    {
        Hits++;
    }
    
    public Ship(String name, Owner own, int hits, int length, boolean isAfloat)
    {
        Name = name;
        Own = own;
        Hits = hits;
        Length = length;
        IsAfloat = isAfloat;
    }
}
