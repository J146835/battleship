/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

/**
 *
 * @author James Watson, Nick Watt
 * 
 * Cell class represents a location on the board
 */
public class Cell {
    
    // Enum for grid marker
    public enum Marker {
        EMPTY("~"),
        HIT("H"),
        MISS("M"),
        SHIP("S");
        
        private final String label;
        
        Marker(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }
    
    public Marker marker;
    public Ship ship;
    
    void guess()
    {
        if(ship != null)
        {
            ship.hit();
            marker = Marker.HIT;
            System.out.println("HIT!");
            ship.checkSink();
        } 
        else 
        {
           marker = Marker.MISS;
           System.out.println("Miss...");
        }
    }
    
    public Cell()
    {
        marker = Marker.EMPTY;
    }
    
    void displayShips()
    {
         if(ship != null)
         {
             marker = Marker.SHIP;
         }
    }
}
