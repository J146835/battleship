/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import battleship.Cell.Marker;
import battleship.Ship.Orientation;
import battleship.Ship.Owner;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author James Watson, Nick Watt
 * 
 * Game class contains the game logic
 */
public class Game {
    
    // the size of the boards
    public static final int len = 9;
    // alphabet for translate array indexs to grid coordinates
    public static final char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray(); // Past I is not used
    
    // the boards on which the game takes place
    public static Cell[][] topBoard = new Cell[len][len];
    public static Cell[][] bottomBoard = new Cell[len][len];
    // the lists of ships which act as the game pieces
    public static ArrayList<Ship> playerFleet = new ArrayList<>();
    public static ArrayList<Ship> opponentFleet = new ArrayList<>();
    
    // stores the opponents last hit coordinates for AI
    public static int[] lastHit = new int[]{-1, -1}; 
    // After a random hit followed by a adjacent hit, hitRow/Col is assigned true and returns false when misses
    public static boolean hitRow = false;
    public static boolean hitCol = false;
    
    // Sets up the game
    public static void setup()
    {
        // iterates through each board and assigns a new cell
        for(int i = 0; i<len; i++)
        {
            for(int j = 0; j<len; j++)
            {
                // assigns a new Cell to each position in the array
                bottomBoard[i][j] = new Cell();
                topBoard[i][j] = new Cell();
            }    
        }
        
        // Add ships to the playerFleet
        playerFleet.add(new Ship("Carrier", Owner.PLAYER, 0, 5, true));
        playerFleet.add(new Ship("Battleship", Owner.PLAYER, 0, 4, true));
        playerFleet.add(new Ship("Cruiser", Owner.PLAYER, 0, 3, true));
        playerFleet.add(new Ship("Submarine", Owner.PLAYER, 0, 3, true));
        playerFleet.add(new Ship("Destroyer", Owner.PLAYER, 0, 2, true));
        
        // Add ships to the opponentFleet
        opponentFleet.add(new Ship("Carrier", Owner.OPPONENT, 0, 5, true));
        opponentFleet.add(new Ship("Battleship", Owner.OPPONENT, 0, 4, true));
        opponentFleet.add(new Ship("Cruiser", Owner.OPPONENT, 0, 3, true));
        opponentFleet.add(new Ship("Submarine", Owner.OPPONENT, 0, 3, true));
        opponentFleet.add(new Ship("Destroyer", Owner.OPPONENT, 0, 2, true));
      
        playerPlacement();

        opponentPlacement();
        
        displayShips();
        
        System.out.println(printArray(topBoard));
        System.out.println(printArray(bottomBoard));
        
    }
    
    // makes the cells containing the players ships viewable to the player 
    public static void displayShips()
    {
        for(int i = 0; i<len; i++)
        {
            for(int j = 0; j<len; j++)
            {
                //topBoard[i][j].displayShips(); // uncomment for testing
                bottomBoard[i][j].displayShips();
            }    
        }
    }
    
    // iterates through the opponentFleet randomly placeing each ship on the topboard
    public static void opponentPlacement()
    {
        opponentFleet.stream().forEach((ship) -> {
            boolean placed = false;
            
            while(!placed)
            {
                // max and min for random numbers
                int max = 1;
                int min = 0;
                
                Random randomNum = new Random();
                
                // coin flip for orientation
                int flip = min + randomNum.nextInt(max);
                
                char orient;
                
                if (Math.random() < 0.5)
                {
                    orient = "h".charAt(0);
                }
                else
                {
                    orient = "v".charAt(0);
                }
                
                // change the max value to the ship length
                max = 5;
                min = 1;
                
                int row = 1;
                int col = 1;
                
                if(orient == "h".charAt(0))
                {
                    row = min + randomNum.nextInt(max);
                    max = len - ship.Length;
                    col = min + randomNum.nextInt(max);
                    
                }
                else if(orient == "v".charAt(0))
                {
                    col = min + randomNum.nextInt(max);
                    max = len - ship.Length;
                    row = min + randomNum.nextInt(max);
                }
                placed = placeShip(topBoard, ship, row, col, orient);
            }
        });
    }
    
    // Iterates through the playerFleet allowing the user to input values to place each ship on the bottomboard
    public static void playerPlacement()
    {
        for(Ship ship : playerFleet)
        {
            boolean placed = false;
            
            // displays the board for the user
            System.out.println(printArray(bottomBoard));
            
            while(!placed)
            {
                // This section should be replaced with a single string input from the user and code splitting and validating it
                Scanner in = new Scanner(System.in);
                System.out.println("Please enter the coordinates for the " + ship.Name + ":\n(V for Vertical, H for Horizontal; For Vertical A - " + indexLetter(len - ship.Length) + ", "
                + "0 - " + (len - 1) + "; For Horizontal A - " + indexLetter(len - 1) + ", 0 - " + (len - ship.Length) + ")");

                String input = in.nextLine().toLowerCase();
                
                // prevents invalid inputs
                while(input.isEmpty() || !isValidPlace(input))
                {
                    System.out.println("Your input is invalid, please try again.");
                    input = in.nextLine().toLowerCase();
                }
                
                char orient = input.charAt(0);
                char row = input.charAt(1);
                int col = Integer.parseInt(Character.toString(input.charAt(2))) - 1;
                
              
                //System.out.println(letterIndex(row) + " : " + col + " : " + orient);
                placed = placeShip(bottomBoard, ship, letterIndex(row), col, orient);

                if(placed)
                {
                  System.out.println(ship.Name + " has been placed at " + indexLetter(letterIndex(orient)) + " : " + indexLetter(letterIndex(row)) + " : " + (col+1));  
                }
                else
                {
                    System.out.println("That position is unavailable, please try again");
                }

                displayShips();
            }  
        }
        separator();
    }
    
    // for formatting
    public static void separator()
    {
        System.out.println("#############################################\n");
    }
    
    // Places the ships
    public static boolean placeShip(Cell[][] board, Ship ship, int row, int col, char orient)
    {
        if(orient == "h".charAt(0))
        {
            ship.Orient = Orientation.HORIZONTAL;
        }
        else if(orient == "v".charAt(0))
        {
            ship.Orient = Orientation.VERTICAL;
        }
        
        
        if(ship.Orient == Orientation.VERTICAL && row <= (len - ship.Length))
        {
            for (int i=0; i < ship.Length; i++) // removed - 1 after ship length
            {
                if(board[row][col].ship == null)
                {
                    //System.out.println(row + " " + col);
                    board[row][col].ship = ship; // there seems to be an error with the Destroyer ship here - check math later
                    row++;
                }
                else
                {
                    //System.out.println(board[row][col].ship.Name); - I have forgotten what this was for
                    return false;
                }

            }
            return true;
        }
        else if(ship.Orient == Orientation.HORIZONTAL && col < (len - ship.Length))
        {
            for (int i=0; i < ship.Length; i++) // removed - 1 after ship length
            {
                if(board[row][col].ship == null)
                {
                    board[row][col].ship = ship;
                    col++;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    // Check if the game is over
    public static boolean checkGame(ArrayList<Ship> fleet)
    {
        int shipCount = fleet.size();
        
        shipCount = fleet.stream().filter((ship) -> (!ship.IsAfloat)).map((_item) -> 1).reduce(shipCount, (accumulator, _item) -> accumulator - 1); // Netbeans wanted this
        return shipCount > 0;
    }
    
    // Prints the board to the console
    public static String printArray(Cell[][] board)
    {   
        String output = "      1 2 3 4 5 6 7 8 9\n  \u2554\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2557\n";
        for(int i = 0; i < len; i++)
        {
            output += alphabet[i] + " \u2551 ";
            for(int j = 0; j < len; j++)
            {
                output += " " + board[i][j].marker.toString();
            }
            output += " \u2004\u2551\n"; //THREE-PER-EM SPACE here
        }
        output += "  \u255A\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u255D\n";
        return output;
    }
    
    // Opponent's guess
    public static void opponentGuess()
    {
        int max = len;
        int min = 0;
        Random randomNum = new Random();
        
        boolean guess = false;
        boolean eGuess = false;
        
        do
        {
            // random guess
            int gRow = min + randomNum.nextInt(max);
            int gCol = min + randomNum.nextInt(max);
            
            // if last turn was a hit
            if(lastHit[0] >= 0 && lastHit[1] >= 0)
            {
                // Coin flips for randomization
                int flip1 = randomNum.nextInt(2);
                int flip2 = randomNum.nextInt(2);
                
                // if hit was followed by a successful adjacent guess, follows either col or row with direction based on coinflips until a miss
                if(hitRow == true)
                {
                    if(flip1 == 0 && (lastHit[0] + 1) < max && (bottomBoard[lastHit[0] + 1][gCol].marker == Marker.EMPTY || bottomBoard[lastHit[0] + 1][gCol].marker == Marker.SHIP))
                    {
                        gRow = lastHit[0] + 1;
                        gCol = lastHit[1];
                        eGuess = true;
                    }
                    else if(flip1 == 1 && (lastHit[0] - 1) > 0 && (bottomBoard[lastHit[0] - 1][gCol].marker == Marker.EMPTY || bottomBoard[lastHit[0] - 1][gCol].marker == Marker.SHIP))
                    {
                        gRow = lastHit[0] - 1;
                        gCol = lastHit[1]; 
                        eGuess = true;
                    }
                    else
                    {
                       eGuess = false; 
                    }
                }
                else if(hitCol == true)
                {
                    if(flip1 == 0 && (lastHit[1] + 1) < max && (bottomBoard[gRow][lastHit[1] + 1].marker == Marker.EMPTY || bottomBoard[lastHit[0] + 1][gCol].marker == Marker.SHIP))
                    {
                        gRow = lastHit[0];
                        gCol = lastHit[1] + 1;
                        eGuess = true;
                    }
                    else if(flip1 == 1 && (lastHit[1] - 1) > 0 && (bottomBoard[gRow][lastHit[1] - 1].marker == Marker.EMPTY || bottomBoard[lastHit[0] + 1][gCol].marker == Marker.SHIP))
                    {
                        gRow = lastHit[0];
                        gCol = lastHit[1] - 1;
                        eGuess = true;
                    }
                    else
                    {
                       eGuess = false; 
                    }
                }
                
                // determines if following hits in a row or col (should have documented this section better)
                if(flip1 == 0 && eGuess == false)
                {
                    if((lastHit[0] - 1) >= 0 && (lastHit[0] + 1) < max)
                    {
                        if(flip2 == 0 && (bottomBoard[lastHit[0] + 1][gCol].marker == Marker.EMPTY || bottomBoard[lastHit[0] + 1][gCol].marker == Marker.SHIP))
                        {
                            gRow = lastHit[0] + 1;
                            gCol = lastHit[1];
                            
                            if(bottomBoard[lastHit[0] + 1][gCol].marker == Marker.SHIP)
                            {
                                System.out.print("I am going for the same column directly below!\n");
                            }
                             
                            hitRow = bottomBoard[gRow][gCol].marker == Marker.SHIP;
                            
                        }
                        else if(flip2 == 1 && (bottomBoard[lastHit[0] - 1][gCol].marker == Marker.EMPTY || bottomBoard[lastHit[0] - 1][gCol].marker == Marker.SHIP))
                        {
                            gRow = lastHit[0] - 1;
                            gCol = lastHit[1];
                            
                            if(bottomBoard[lastHit[0] - 1][gCol].marker == Marker.SHIP)
                            {
                                System.out.print("I am going for the same column directly above!\n");
                            }

                            hitRow = bottomBoard[gRow][gCol].marker == Marker.SHIP;
                        }
                    }
                }
                else if (flip1 == 1 && eGuess == false)
                {
                    if((lastHit[1] - 1) >= 0 && (lastHit[1] + 1) < max)
                    {
                        if(flip2 == 0 && bottomBoard[gRow][lastHit[1] + 1].marker == Marker.EMPTY || bottomBoard[gRow][lastHit[1] + 1].marker == Marker.SHIP)
                        {
                            gCol = lastHit[1] + 1;
                            gRow = lastHit[0];
                            
                            if(bottomBoard[gRow][lastHit[1] + 1].marker == Marker.SHIP)
                            {
                                System.out.print("I am going for the same row to the right!\n");
                            }      
                            
                            hitCol = bottomBoard[gRow][gCol].marker == Marker.SHIP;
                        }
                        else if(flip2 == 1 && bottomBoard[lastHit[1] - 1][gCol].marker == Marker.EMPTY || bottomBoard[gRow][lastHit[1] - 1].marker == Marker.SHIP)
                        {
                            gCol = lastHit[1] - 1;
                            gRow = lastHit[0];
                            
                            if(bottomBoard[gRow][lastHit[1] - 1].marker == Marker.SHIP)
                            {
                                System.out.print("I am going for the same row to the left!\n");
                            }
                            
                            hitCol = bottomBoard[gRow][gCol].marker == Marker.SHIP;
                        }
                    }
                } 
            }
            
            // Some extra RNG for fun
            int rng = min + randomNum.nextInt(31);
            
            // rng is 0 and not in eGuess (successive hits down row or column) 
            if(rng == 0 && eGuess == false)
            {
                loop:
                for(int i = 0; i<len; i++)
                {
                    for(int j = 0; j<len; j++)
                    {
                        if(bottomBoard[i][j].marker == Marker.SHIP)
                        {
                            System.out.print("I never miss!\n");
                            gRow = i;
                            gCol = j;
                            break loop;
                        }   
                    }    
                }
            }
            
            
            // this is where the guess takes place
            if(bottomBoard[gRow][gCol].marker == Marker.EMPTY || bottomBoard[gRow][gCol].marker == Marker.SHIP)
            {
                System.out.println("Your opponent fired at " + indexLetter(gRow) + ":" + (gCol+1) + "!");
                bottomBoard[gRow][gCol].guess();
                if(bottomBoard[gRow][gCol].marker == Marker.HIT)
                {
                    lastHit[0] = gRow;
                    lastHit[1] = gCol;
                }
                else
                {
                    lastHit[0] = -1;
                    lastHit[1] = -1;
                }
                guess = true;
            }    
        } while(!guess);  
    }
    
    // Player's guess
    public static void playerGuess()
    {
        
        boolean guess = false; // when true loop ends
        boolean repeat = false; // trigger for additional dialog
        
        Scanner in = new Scanner(System.in);
        
        do
        {
            // input validation need here, input maybe changed to a single string
            System.out.println("Please enter your attack coordinates: ('A-1''1-9')");
            String input = in.nextLine().toLowerCase();
            
            // prevents invalid inputs
            while(input.isEmpty() || !isValidGuess(input)){
                System.out.println("Your input is invalid, please try again:");
                input = in.nextLine().toLowerCase();
            }
            
            char row = input.charAt(0);
            int  col = Integer.parseInt(Character.toString(input.charAt(1))) - 1; // seriously?
            
            if(letterIndex(row) < len && col >= 0)
            {
                if(topBoard[letterIndex(row)][col].marker == Marker.EMPTY || topBoard[letterIndex(row)][col].marker == Marker.SHIP)
                {
                    System.out.println("You fired at " + (Character.toUpperCase(row)) + ":" + (col+1) + "!");
                    topBoard[letterIndex(row)][col].guess();
                    guess = true;
                }
                else
                {
                    repeat = true;
                }
            }
            else
            {
                repeat = true;
            }
   
            if(repeat)
            {
                System.out.println("You have already attacked these coordinates, please try again:");
            }
            repeat = true;
        } while(!guess);  
    }
    
    // validates user's guess input
    public static boolean isValidGuess(String input)
    {
        if(input.length() >= 2)
        {
            return Character.isLetter(input.charAt(0)) && Character.isDigit(input.charAt(1));
        }
        return false;
    }
    
    // validates user's place input
    public static boolean isValidPlace(String input)
    {
        if(input.length() >= 3)
        {
            return Character.isLetter(input.charAt(0)) && Character.isLetter(input.charAt(1)) && Character.isDigit(input.charAt(2));
        }
        return false;
    }
    
    // returns an int which corresponds to the letters position in the alphabet - 1 for use with column index
    public static int letterIndex(char ch)
    {
        int pos = ch - 'a';// + 1 for actual alphabet position;
        return pos;
    }
    
    // returns the letter which corresponds to the int given for use with column index
    public static String indexLetter(int i) {
        return Character.toString(alphabet[i]);
    }
}
